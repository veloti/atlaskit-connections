import React from 'react';
import { BreadcrumbsStateless, BreadcrumbsItem } from '@atlaskit/breadcrumbs';

interface Props {
    connectionName: any;
}

export default class BreadcrumbsExpand extends React.Component<
    Props,
    { isExpanded: boolean }
    > {
    state = {
        isExpanded: false,
    };

    expand(e: React.MouseEvent) {
        e.preventDefault();
        this.setState({ isExpanded: true });
    }

    render() {
        return (
            <BreadcrumbsStateless
                isExpanded={this.state.isExpanded}
                onExpand={e => this.expand(e)}
            >
                <BreadcrumbsItem href="/" text="Connections" />
                <BreadcrumbsItem text={this.props.connectionName} />
            </BreadcrumbsStateless>
        );
    }
}