import React from 'react';

import TableTree from '@atlaskit/table-tree';


const Title = props => <span>{props.title}</span>;
const Numbering = props => <span>{props.numbering}</span>;

const SectionTable = () => (
    <TableTree
        headers={['Rule name', 'Assignee', 'Action', 'Delete']}
        columns={[Title, Numbering]}
        columnWidths={["25%","25%","25%","25%"]}

    />
);

export default SectionTable