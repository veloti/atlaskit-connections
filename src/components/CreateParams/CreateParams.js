import React from "react";
import './CreateParams.css';
import Toggle from '@atlaskit/toggle';


const CreateParams = props => {

    return (
        <div className={"Main"}>
            <div>
                <div><span>Enable connection</span></div>
                <div><Toggle size="large" isDefaultChecked /></div>
            </div>
            <div>
                Params: 2
            </div>
            <div>
                Params: 3
            </div>
        </div>
        // <div className={"Main"}>
        //     <div>
        //         <label>Example</label>
        //     </div>
        // </div>
    )
}

export default CreateParams