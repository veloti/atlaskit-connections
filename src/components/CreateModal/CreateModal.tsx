import React from 'react';
import Button from '@atlaskit/button';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import CreateParams from "../CreateParams/CreateParams";
import {
    MenuGroup,
    Section,
    HeadingItem,
    LinkItem,
    ButtonItem
} from '@atlaskit/menu';
import FormContent from "../FormContent/FormContent";

interface State {
    isOpen: boolean;
}
export default class CreateModal extends React.PureComponent<{}, State> {
    state: State = { isOpen: false };

    open = () => this.setState({ isOpen: true });

    close = () => this.setState({ isOpen: false });

    secondaryAction = ({ target }: any) => console.log(target.innerText);

    render() {
        const { isOpen } = this.state;
        const actions = [
            { text: 'Close', onClick: this.close },
            { text: 'Secondary Action', onClick: this.secondaryAction },
        ];

        return (
            <div>
                <ButtonItem onClick={this.open}>Create connection</ButtonItem>

                <ModalTransition>
                    {isOpen && (
                        <Modal actions={actions} onClose={this.close} heading="Modal Title">
                            <div>
                                AAAAAA
                            </div>
                            <hr/>
                            <CreateParams/>
                            <hr/>
                            <FormContent/>

                        </Modal>
                    )}
                </ModalTransition>
            </div>
        );
    }
}