import React from 'react';
import SectionMessage from '@atlaskit/section-message';
import Button, { ButtonGroup } from '@atlaskit/button';

const SectionText = () => (
    <SectionMessage
        title="Manage your rules"
        actions={[
            {
                key: 'learn',
                href: 'https://en.wikipedia.org/wiki/Mary_Shelley',
                text: 'Learn about Alert Cather rules',
            },
            {
                key: 'readabout',
                href: 'https://en.wikipedia.org/wiki/Villa_Diodati',
                text: 'Read about actions',
            },
            {
                key: 'readhow',
                href: 'https://en.wikipedia.org/wiki/Mary_Shelley',
                text: 'Read how to create incidents',
            },
        ]}
    >
        <p>
            Group alerts coming  from third-party systems, create rules and
            assign users. Please make sure that the integration field is added
            to the issue screens for incident's issue type.
        </p>
        <p>
            Your REST endpoint: "http://example.com/"
        </p>
        <div>
            <Button>Create rule</Button>
        </div>

    </SectionMessage>
);

export default SectionText;