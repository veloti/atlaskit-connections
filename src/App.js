import React, {Component} from 'react';
import axios from 'axios';
import Spinner from '@atlaskit/spinner';
import StartMenu from "./containers/StartMenu/StartMenu";
import {Route} from 'react-router-dom';
import ConnectionPage from "./containers/ConnectionPage/ConnectionPage";

import './App.css';



class App extends Component {
    state = {
        connections: [],
    }

    async componentDidMount() {
        try {
            const response = await axios.get(`https://connections-e79e9.firebaseio.com/connectionstest2.json`)
            const connections = response.data

            this.setState({connections})
        } catch (e) {
            console.log(e)
        }

        console.log(this.state.connections)
    }

    render() {
        let routes = null;

        routes = this.state.connections.map((connection, index) => {
            return (
                <Route
                    key={index}
                    path={`/${connection.id}`}
                    render={() => <ConnectionPage
                        connection={connection}
                    />}
                />
            )
        })

        console.log(routes)
        return(
            this.state.connections.length
                ?
                    <>
                        {routes}
                        <Route path="/" exact render={() =>
                            <StartMenu
                                connections={this.state.connections}
                                update
                            />}
                        />
                    </>
                : <Spinner size="xlarge" />
        )
    }
}

export default App;
