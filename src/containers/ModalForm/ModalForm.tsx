import React, { Fragment } from 'react';
import Button from '@atlaskit/button';

import Form, { Field, CheckboxField, ErrorMessage } from '@atlaskit/form';
import { Checkbox } from '@atlaskit/checkbox';
import Textfield from '@atlaskit/textfield';
//import RadioGroup, { AkRadio } from '@atlaskit/field-radio-group';
import { RadioGroup, Radio } from '@atlaskit/radio';

import ModalDialog, { ModalFooter, ModalTransition } from '@atlaskit/modal-dialog';
// import { FooterProps } from '../src/components/Footer';
import Select, { ValueType } from '@atlaskit/select';

import {
    MenuGroup,
    Section,
    HeadingItem,
    LinkItem,
    ButtonItem
} from '@atlaskit/menu';
import axios from 'axios'

interface OptionType {
    label: string;
    value: string;
}

interface State {
    isOpen: boolean;
}

interface Props {
    selectOption: any;
}

// function dataValidator(data: object) {
//     for(let key in data) {
//         if (data[key]) {
//
//         }
//     }
// }

async function dataValidator2(data: any) {
    // const data2 = Object.assign({}, data);
    // for(let key in data2) {
    //     console.log(`${key}: ${data2[key]}`)
    // }
    for(let key in data) {
        if (data[key] === null) {
            alert("Fill in all of the fields")
            return false
        }
        if (data[key].value) {
            data[key] = data[key].value
        }
    }

    try {
        await axios.post('https://connections-e79e9.firebaseio.com/posttest.json', data)
    } catch (e) {
        console.log(e)
    }

    return true
}

const colors = [
    { label: 'blue', value: 'blue' },
    { label: 'red', value: 'red' },
    { label: 'purple', value: 'purple' },
    { label: 'black', value: 'black' },
    { label: 'white', value: 'white' },
    { label: 'gray', value: 'gray' },
    { label: 'yellow', value: 'yellow' },
    { label: 'orange', value: 'orange' },
    { label: 'teal', value: 'teal' },
    { label: 'dog', value: 'dog' },
];

const selectOption = [
    { label: 'Adelaide', value: 'adelaide' },
    { label: 'Brisbane', value: 'brisbane' },
    { label: 'Canberra', value: 'canberra' },
    { label: 'Darwin', value: 'darwin' },
    { label: 'Hobart', value: 'hobart' },
    { label: 'Melbourne', value: 'melbourne' },
    { label: 'Perth', value: 'perth' },
    { label: 'Sydney', value: 'sydney' },
];

export default class ModalForm extends React.Component<Props, State> {
    state = { isOpen: false };

    open = () => this.setState({ isOpen: true });

    close = () => this.setState({ isOpen: false });

    onFormSubmit = (data: Object) => {
        //console.log(JSON.stringify(data))
        if (dataValidator2(data)) {
            this.close()
        }
        console.log(JSON.stringify(data))
    };

    render() {
        const { isOpen } = this.state;
        const footer = () => (
            <ModalFooter showKeyline={false} >
                <span />
                <div>
                <Button appearance="primary" type="submit">
                    Submit to Console
                </Button>
                <Button appearance="default" onClick={this.close}>
                    Close
                </Button>
                </div>
            </ModalFooter>
        );

        interface ContainerProps {
            children: React.ReactNode;
            className?: string;
        }

        return (
            <div>
                <ButtonItem onClick={this.open}>Create connection</ButtonItem>

                <ModalTransition>
                    {isOpen && (
                        <ModalDialog
                            heading="Form Demo"
                            onClose={this.close}
                            components={{
                                Container: ({ children, className }: ContainerProps) => (
                                    <Form onSubmit={this.onFormSubmit}>
                                        {({ formProps }) => (
                                            <form {...formProps} className={className}>
                                                {children}
                                            </form>
                                        )}
                                    </Form>
                                ),
                                Footer: footer,
                            }}
                        >
                            <CheckboxField name="enabled" defaultIsChecked>
                                {({ fieldProps }) => (
                                    <Checkbox {...fieldProps} value="example" label="Enable connection" />
                                )}
                            </CheckboxField>
                            <Field label="Connection name" name="connectionName" defaultValue="">
                                {({ fieldProps }) => <Textfield {...fieldProps} />}
                            </Field>

                            <Field<ValueType<OptionType>>
                                name="integrationName"
                                label="Project"
                                defaultValue={this.props.selectOption[1]}
                            >
                                {({ fieldProps: { id, ...rest }, error }) => (
                                    <Fragment>
                                        <Select<OptionType>
                                            validationState={error ? 'error' : 'default'}
                                            inputId={id}
                                            {...rest}
                                            options={this.props.selectOption}
                                            // isClearable
                                        />
                                        {error && <ErrorMessage>{error}</ErrorMessage>}
                                    </Fragment>
                                )}
                            </Field>

                            <Field<ValueType<OptionType>>
                                name="incidentIssueType"
                                label="Issue type for incident"
                                defaultValue={null}
                            >
                                {({ fieldProps: { id, ...rest }, error }) => (
                                    <Fragment>
                                        <Select<OptionType>
                                            validationState={error ? 'error' : 'default'}
                                            inputId={id}
                                            {...rest}
                                            options={[
                                                { label: 'Adelaide', value: 'adelaide' },
                                                { label: 'Brisbane', value: 'brisbane' },
                                            ]}
                                            // isClearable
                                        />
                                        {error && <ErrorMessage>{error}</ErrorMessage>}
                                    </Fragment>
                                )}
                            </Field>

                            <Field<ValueType<OptionType>>
                                name="reporter"
                                label="Default reporter"
                                defaultValue={null}
                            >
                                {({ fieldProps: { id, ...rest }, error }) => (
                                    <Fragment>
                                        <Select<OptionType>
                                            validationState={error ? 'error' : 'default'}
                                            inputId={id}
                                            {...rest}
                                            options={colors}
                                            // isClearable
                                        />
                                        {error && <ErrorMessage>{error}</ErrorMessage>}
                                    </Fragment>
                                )}
                            </Field>

                            <CheckboxField name="anonymous_allowed" defaultIsChecked>
                                {({ fieldProps }) => (
                                    <Checkbox {...fieldProps} value="example" label="Allow anonymous requests" />
                                )}
                            </CheckboxField>

                            <CheckboxField name="get_reqeusts_allowed" defaultIsChecked>
                                {({ fieldProps }) => (
                                    <Checkbox {...fieldProps} value="example" label="Creating incidents via GET requests" />
                                )}
                            </CheckboxField>

                            <Field label="Restrict API calls for IP" name="allowed_ip" defaultValue="">
                                {({ fieldProps }) => <Textfield {...fieldProps} />}
                            </Field>

                            {/*<Field name="radiogroup" defaultValue="">*/}
                            {/*    {({ fieldProps: { value, ...others } }) => (*/}
                            {/*        <RadioGroup*/}
                            {/*            items={radioItems}*/}
                            {/*            label="Basic Radio Group Example"*/}
                            {/*            {...others}*/}
                            {/*        >*/}
                            {/*            <Radio name="standalone" value="singleButton">*/}
                            {/*                Radio button*/}
                            {/*            </Radio>*/}
                            {/*        </RadioGroup>*/}
                            {/*    )}*/}
                            {/*</Field>*/}
                        </ModalDialog>
                    )}
                </ModalTransition>
            </div>
        );
    }
}