import React from "react";
import BreadcrumbsExpand from "../../components/Breadcrumb/Breadcrumb";
import classes from "./ConnectionPage.module.css";
import PageHeader from '@atlaskit/page-header';
import Button, { ButtonGroup } from '@atlaskit/button';
import SectionText from "../../components/SectionText/SectionText";
import SectionTable from "../../components/SectionTable/SectionTable";

const ConnectionPage = props => {

    return(
        <div className={classes.Main}>
            <div>
                <BreadcrumbsExpand
                    connectionName={props.connection.integrationName}
                />
            </div>
            <div className={classes.HeaderFlex}>
            <label className={classes.Header}>{props.connection.integrationName}</label>
            <div className={classes.ButtonGroup}><Button>Setting</Button><Button>Delete</Button></div>
            </div>
            <hr/>
            <SectionText/>
            <SectionTable/>
        </div>
    )

}

export default ConnectionPage