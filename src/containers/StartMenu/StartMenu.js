import React from "react";
import classes from './StartMenu.module.css';
import {Link} from "react-router-dom";
import Button, { ButtonGroup } from '@atlaskit/button';
import CreateModal from "../../components/CreateModal/CreateModal";
import {
    MenuGroup,
    Section,
    HeadingItem,
    LinkItem,
    ButtonItem
} from '@atlaskit/menu';
import VidConnectionCircleIcon from '@atlaskit/icon/glyph/vid-connection-circle';
import ModalForm from "../ModalForm/ModalForm";

const StartMenu = props => {
    console.log(props)
    let connectionList = props.connections.map((item, index) => {
        return (
            <Link to={`/${item.id}`} className={classes.underlinenone}>
                <div className={classes.connectionBox}>
                    <div className={classes.label}>{item.integrationName}</div>
                </div>
            </Link>
        )

    })

        let connections = null;
        connections = props.connections.map((connection, index) => {
            return (
                <LinkItem
                    style={{textDecoration: 'none'}}
                    // description={`Description for ${connection.name}`}
                    elemBefore={<VidConnectionCircleIcon/>}
                    //LINK HERE
                    href={connection.id}
                >
                    {connection.integrationName}
                </LinkItem>
            )
        })



    return (
        // <div className={classes.Main}>
        //     <div className={classes.header}>
        //         <div className={classes.headerLabel}>Connections</div>
        //         <CreateModal/>
        //     </div>
        //     <hr/>
        //     {connectionList}
        // </div>
        //------------------------
        // <div className={classes.Main}>
            <MenuGroup>
                <Section>
                    <HeadingItem>Connections</HeadingItem>
                    {connections}
                </Section>
                <Section hasSeparator>
                    <ModalForm
                        selectOption={[
                            { label: 'Adelaide', value: 'adelaide' },
                            { label: 'Brisbane', value: 'brisbane' },
                            { label: 'Canberra', value: 'canberra' },
                        ]}
                    />
                </Section>
            </MenuGroup>
        // </div>
    )


}

export default StartMenu